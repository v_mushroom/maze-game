/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze;

import com.mush.game.utils.swing.Game;

/**
 *
 * @author mush
 */
public class Main {

    public static void main(String[] args) {
        Game.setupLogging();

        MainContent content = new MainContent();
        Game game = content.engine;

        game.frameTitle = "Maze";
        game.preferredSize.setSize(content.renderer.getScreenWidth(), content.renderer.getScreenHeight());
//        game.setFrameUndecorated(true);
//        game.setUseActiveGraphics(true);
        game.setPauseOnLoseFocus(true);
        game.start();

        content.renderer.showFps(true);
//        game.getRefreshThread().setTargetFps(60);
//        game.getRefreshThread().setTimekeepingInsanity(true);
    }

}
