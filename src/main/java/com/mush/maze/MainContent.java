/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze;

import com.mush.maze.game.Content;
import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.GameKeyboard;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.render.DelegatingGameRenderer;
import com.mush.game.utils.swing.Game;
import com.mush.game.utils.swing.GameKeyListener;
import com.mush.maze.game.Renderer;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;

/**
 *
 * @author mush
 */
public class MainContent implements Updateable {

    public enum NavigationEvent {
        QUIT
    }

    public final DelegatingGameRenderer renderer;
    public final Game engine;
    public final GameKeyListener keyListener;

    private Config config;
    private Content gameContent;

    public MainContent() {
        // TODO load, no rush
        config = new Config();
        
        renderer = new DelegatingGameRenderer(config.screenWidth, config.screenHeight);
        keyListener = new GameKeyListener();
        engine = new Game(this, renderer, keyListener);

        GameKeyboard gameKeyboard = new GameKeyboard(GameEventQueue.Categories.SYSTEM);
        gameKeyboard.bindActionKey(KeyEvent.VK_F4, MainContent.NavigationEvent.QUIT);
        keyListener.addKeyboard(gameKeyboard);

        GameEventQueue.category(GameEventQueue.Categories.SYSTEM).addEventListener(this);

        gameContent = new Content(config, keyListener);
        renderer.setRenderable(new Renderer(gameContent));
//        renderer.showFpsHistory(true);

        renderer.setPixelScale(config.pixelScale);
//        renderer.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//        renderer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//        renderer.applyRenderingHints();
    }

    @OnGameEvent
    public void onAction(MainContent.NavigationEvent navigationEvent) {
        switch (navigationEvent) {
            case QUIT:
                engine.close();
                break;
        }
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof MainContent.NavigationEvent) {
            switch ((MainContent.NavigationEvent) action.message) {
                case QUIT:
                    GameEventQueue.category(GameEventQueue.Categories.SYSTEM).sendEvent(MainContent.NavigationEvent.QUIT);
                    break;
            }
        }
    }

    @Override
    public void update(double elapsedSeconds) {
        gameContent.update(elapsedSeconds);
        GameEventQueue.updateDelayed(elapsedSeconds);
        GameEventQueue.processQueue();
    }

    @Override
    public void updateCurrentFps(double fps) {
    }
}
