/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze;

/**
 *
 * @author mush
 */
public class Config {

    public int screenWidth = 512;
    public int screenHeight = 512;
    public double pixelScale = 2;

    public int getUnscaledScreenWidth() {
        return (int) (screenWidth / pixelScale);
    }

    public int getUnscaledScreenHeight() {
        return (int) (screenHeight / pixelScale);
    }
}
