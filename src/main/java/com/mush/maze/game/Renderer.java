/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze.game;

import com.mush.game.utils.map.move.Box;
import com.mush.game.utils.render.GameRenderer;
import com.mush.game.utils.render.Renderable;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 *
 * @author mush
 */
public class Renderer implements Renderable {

    private Content content;

    public Renderer(Content mazeContent) {
        this.content = mazeContent;
    }

    @Override
    public void renderContent(Graphics2D g, GameRenderer renderer) {
        if (content.map == null) {
            return;
        }

        if (content.show) {
            g.setColor(Color.DARK_GRAY);
            g.fillRect(0, 0, renderer.getScreenWidth(), renderer.getScreenHeight());
        } else {
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, renderer.getScreenWidth(), renderer.getScreenHeight());
            return;
        }

        content.map.surface.draw(g);

        AffineTransform t = g.getTransform();
        g.translate(-content.camera.getxOffset(), -content.camera.getyOffset());

        content.mobs.getAll().forEach((mob) -> {
            if (mob.sprite != null) {
                mob.sprite.draw(g, (int) mob.position.getX(), (int) mob.position.getY());
            } else {
                drawBox(g, mob.movingBox);
            }
        });

        g.setTransform(t);
    }

    private void drawBox(Graphics2D g, Box box) {
        int x = (int) box.left;
        int y = (int) box.top;
        int w = (int) (box.right - box.left - 1);
        int h = (int) (box.bottom - box.top - 1);

        g.setColor(Color.YELLOW);
        g.fillRect(x, y, w, h);
        g.setColor(Color.RED);
        g.drawRect(x, y, w, h);
    }
}
