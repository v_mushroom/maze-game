/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze.game;

import com.mush.maze.game.map.MultiMap;
import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.move.MovingObject;
import com.mush.game.utils.map.move.MovingObjects;
import com.mush.game.utils.render.TrailingTargetCamera;
import com.mush.game.utils.sprites.SpriteFactory;
import com.mush.game.utils.swing.GameKeyListener;
import com.mush.maze.Config;
import com.mush.maze.game.map.MapSwitchEvent;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class Content implements Updateable {

    public MovingObject playerMob;
    public MovingObjects<MapMaterial> mobs;
    public MultiMap map;
    private PlayerInput input;
    public TrailingTargetCamera camera;
    public boolean show = true;
    private SpriteFactory spriteFactory;

    public Content(Config config, GameKeyListener keyListener) {
        input = new PlayerInput(keyListener);

        GameEventQueue.category(GameEventQueue.Categories.INPUT).addEventListener(this);

        mobs = new MovingObjects<>();

        playerMob = new MovingObject();
        playerMob.bounds.set(5, 5, 5, 5);
        mobs.add(playerMob);

        /*
        MovingObject obstacleMob = new MovingObject();
        obstacleMob.bounds.set(0, 0, 40, 40);
        obstacleMob.position.setLocation(100, 10);
        mobs.add(obstacleMob);
         */
        try {
            spriteFactory = new SpriteFactory();
            spriteFactory.load("res/sprites.json");
            playerMob.sprite = spriteFactory.createSprite("PLAYER");
            playerMob.sprite.setState("default");

            map = new MultiMap(config);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, "error", e);
        }

//        camera = new CenteredCamera(250, 150); // maincontent
//        camera.setXBounds(0, 250/2);
//        camera.setYBounds(0, 150/2);
        camera = new TrailingTargetCamera(config.getUnscaledScreenWidth(), config.getUnscaledScreenHeight());
        camera.setLeadingRange(50);
    }

    @Override
    public void update(double elapsedSeconds) {
        if (map == null) {
            return;
        }

        if (playerMob.id != null) {
            double speed = 60;
            playerMob.velocity.setLocation(-input.leftRight.getOutput() * speed, -input.frontBack.getOutput() * speed);
        }

        List<Integer> removeList = new LinkedList<>();

        mobs.getAll().forEach((mob) -> {
            if (mob.sprite != null) {
                mob.sprite.update(elapsedSeconds);
                if (mob.sprite.isAnimationFinished()) {
                    removeList.add(mob.id);
                }
            }
        });

        mobs.beforeCollisionChecking(elapsedSeconds);

        mobs.checkMobCollisions((mob, collisions) -> {
            Logger.getGlobal().log(Level.INFO, "Mob collisions of {0} : {1}", new Object[]{mob.id, collisions});
        });

        mobs.checkMapCollisions(map.contentCollision, (mob, collisions) -> {
            Logger.getGlobal().log(Level.INFO, "Map collisions of {0} :", mob.id);
            collisions.forEach((collision) -> {
                Logger.getGlobal().log(Level.INFO, "with: {0} at {1}, {2}", new Object[]{collision.mapItem.name, collision.u, collision.v});
            });
        });

        mobs.afterCollisionChecking();

        removeList.forEach((mobId) -> mobs.remove(mobId));
        removeList.clear();

        camera.setTarget(playerMob.position.getX(), playerMob.position.getY());
        camera.update(elapsedSeconds);

        Point2D.Double surfacePosition = map.surface.getPosition();

        map.surface.move(-camera.getxOffset() - surfacePosition.x, -camera.getyOffset() - surfacePosition.y);
        map.surface.update(elapsedSeconds);
    }

    @Override
    public void updateCurrentFps(double fps) {
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof PlayerInput.Event) {
            switch ((PlayerInput.Event) action.message) {
                /*
                case SWITCH:
                    Logger.getGlobal().log(Level.INFO, "switch");
                    MapSwitchEvent mse = MapSwitchEvent.PLAYER_DISAPPEAR;
                    GameEventQueue.delayed(GameEventQueue.Categories.INPUT).sendEvent(mse, mse.getDelay());
                    break;
                */
            }
        }
    }

    @OnGameEvent
    public void onMapSwitchEvent(MapSwitchEvent event) {
        Logger.getGlobal().log(Level.INFO, "Map switch event: {0}", event);
        switch (event) {
            case PLAYER_DISAPPEAR:
                if (playerMob.id != null) {
                    mobs.remove(playerMob.id);
                    MovingObject puff = new MovingObject();
                    puff.bounds.set(5, 5, 5, 5);
                    puff.sprite = spriteFactory.createSprite("PLAYER_DISAPPEAR");
                    puff.sprite.setState("default");
                    puff.position.setLocation(playerMob.position);
                    puff.collidesWithMobs = false;
                    mobs.add(puff);
                }
                break;
            case MAP_FADE_OUT:
                switchMap();
                show = false;
                break;
            case MAP_FADE_IN:
                show = true;
                break;
            case PLAYER_REAPPEAR:
                if (playerMob.id == null) {
                    mobs.add(playerMob);

                    MovingObject puff = new MovingObject();
                    puff.bounds.set(5, 5, 5, 5);
                    puff.sprite = spriteFactory.createSprite("PLAYER_APPEAR");
                    puff.sprite.setState("default");
                    puff.position.setLocation(playerMob.position);
                    puff.collidesWithMobs = false;
                    mobs.add(puff);
                }
                break;
        }
        MapSwitchEvent nextEvent = event.nextEvent();
        if (nextEvent != null) {
            GameEventQueue.delayed(GameEventQueue.Categories.INPUT).sendEvent(nextEvent, nextEvent.getDelay());
        }
    }

    @OnGameEvent
    public void onMapFlipEvent(PlayerInput.FlipEvent event) {
        Logger.getGlobal().log(Level.INFO, "Map flip event: {0} {1}", new Object[]{event.unit, event.direction});
        map.flipMap(playerMob.position, event.unit, event.direction);
    }

    
    private void switchMap() {
//        map.nextMap();
//        map.flipMap((int) Math.round(playerMob.position.x / 16), (int) Math.round(playerMob.position.y / 16));
        map.flipMap(playerMob.position, MultiMap.UnitDirection.U, 1);
    }

//    map data, sprites etc
}
