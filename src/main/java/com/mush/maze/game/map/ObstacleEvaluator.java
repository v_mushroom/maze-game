/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze.game.map;

import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import com.mush.game.utils.map.move.MapContentObstacleEvaluator;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author mush
 */
public class ObstacleEvaluator implements MapContentObstacleEvaluator<MapMaterial> {

    final Set<MapMaterial> obstacles;

    public ObstacleEvaluator(MapMaterials materials) {
        obstacles = new HashSet<>();
        materials.getMaterialNames().forEach((name) -> {
            MapMaterial material = materials.getMaterialByName(name);
            if (Boolean.TRUE.equals(material.attributes.get("OBSTACLE"))) {
                obstacles.add(material);
            }
        });
    }

    @Override
    public boolean isObstacle(MapMaterial item) {
        return obstacles.contains(item);
    }

}
