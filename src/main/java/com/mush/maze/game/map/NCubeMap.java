/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze.game.map;

import com.mush.game.utils.map.MapContent;

/**
 *
 * @author mush
 * @param <T>
 */
public class NCubeMap<T> {

    public class Slice<T> implements MapContent<T> {

        private final NCubeMap<T> nCube;
        private final int[] zero;
        private final int[] uVector;
        private final int[] vVector;

        public Slice(NCubeMap<T> nCube, int[] zero, int[] uVector, int[] vVector) {
            this.nCube = nCube;
            this.zero = zero;
            this.uVector = uVector;
            this.vVector = vVector;
        }

        public int[] getZero() {
            return zero;
        }
        
        public void setZero(int[] point) {
            System.arraycopy(point, 0, zero, 0, zero.length);
        }

        public int[] getUVector() {
            return uVector;
        }

        public int[] getVVector() {
            return vVector;
        }

        private int[] duplicate(int[] coordinate) {
            int[] copy = new int[coordinate.length];
            System.arraycopy(coordinate, 0, copy, 0, coordinate.length);
            return copy;
        }

        private void addToMultiplied(int[] point, int[] vector, int scale) {
            for (int i = 0; i < point.length; i++) {
                point[i] += vector[i] * scale;
            }
        }

        public int[] uvToCoordinate(int u, int v) {
            int[] coordinate = duplicate(zero);
            addToMultiplied(coordinate, uVector, u);
            addToMultiplied(coordinate, vVector, v);
            return coordinate;
        }

        @Override
        public T getItemAt(int u, int v) {
            return (T) nCube.getItemAt(uvToCoordinate(u, v));
        }

        @Override
        public void setItemAt(int u, int v, T item) {
            nCube.setItemAt(item, uvToCoordinate(u, v));
        }
    }

    /**
     * N, the number of dimensions of the cube
     */
    private int numberOfDimensions;
    /**
     * Size for each dimension
     */
    private int[] sizes;
    private T[] items;

    public NCubeMap(int... sizes) {
        this.numberOfDimensions = sizes.length;
        this.sizes = sizes;
        int numberOfItems = sizes.length == 0 ? 0 : 1;
        for (int size : sizes) {
            numberOfItems *= size;
        }
        items = (T[]) new Object[numberOfItems];
    }

    /**
     * Number of dimensions
     *
     * @return
     */
    public int getN() {
        return numberOfDimensions;
    }

    public int getSize(int i) {
        return sizes[i];
    }

    public T getItemAt(int... coordinate) {
        int i = index(coordinate);
        return i >= 0 ? (T) items[i] : null;
    }

    public void setItemAt(T item, int... coordinate) {
        int i = index(coordinate);
        if (i >= 0) {
            items[i] = item;
        }
    }

    private int index(int... coordinate) {
        int index = 0;
        int step = 1;

        for (int i = 0; i < numberOfDimensions; i++) {
            int u = coordinate[i];
            int size = sizes[i];

            if (u < 0 || u >= size) {
                return -1;
            }

            index += u * step;
            step *= size;
        }

        return index;
    }

    public Slice<T> getSlice(int[] zero, int[] uVector, int[] vVector) {
        return new Slice(this, zero, uVector, vVector);
    }
}
