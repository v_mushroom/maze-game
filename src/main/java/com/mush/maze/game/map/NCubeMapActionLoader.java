/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze.game.map;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class NCubeMapActionLoader {

    public static class ConfigurationSchema {

        public int[] size;
        public ActionSchema[] actions;
    }

    public static class ActionSchema {

        public String action;
        public String material;
        public int[] at;
        public int[] size;
    }

    public static final String ACTION_FILL = "fill";
    public static final String ACTION_FRAME = "frame";

    public static NCubeMap<MapMaterial> loadFromFile(String filePath, MapMaterials materials) throws IOException {
        Logger.getGlobal().log(Level.INFO, "Loading NCube material map from [{0}]", filePath);

        ConfigurationSchema configuration = loadJson(filePath);

        NCubeMap<MapMaterial> nCubeMap = new NCubeMap<>(configuration.size);

        for (ActionSchema action : configuration.actions) {
            applyAction(nCubeMap, materials, action);
        }

        return nCubeMap;
    }

    private static void applyAction(NCubeMap<MapMaterial> nCubeMap, MapMaterials materials, ActionSchema action) {
        MapMaterial material = materials.getMaterialByName(action.material);
        if (material == null) {
            return;
        }
        switch (action.action) {
            case ACTION_FILL:
                fill(nCubeMap, material, action.at, action.size);
                break;
            case ACTION_FRAME:
                frame(nCubeMap, material, action.at, action.size);
                break;
        }
    }

    private static void fill(NCubeMap<MapMaterial> nCubeMap, MapMaterial material, int[] at, int[] size) {
        fill(nCubeMap, material, at, size, 0);
    }

    private static void fill(NCubeMap<MapMaterial> nCubeMap, MapMaterial material, int[] at, int[] size, int d) {
        if (d >= at.length) {
            Logger.getGlobal().log(Level.INFO, "Set at {0}", Arrays.toString(at));
            nCubeMap.setItemAt(material, at);
            return;
        }

        int[] point = new int[at.length];
        System.arraycopy(at, 0, point, 0, point.length);

        for (int i = at[d]; i < at[d] + size[d]; i++) {
            point[d] = i;
            fill(nCubeMap, material, point, size, d + 1);
        }
    }

    private static void frame(NCubeMap<MapMaterial> nCubeMap, MapMaterial materialByName, int[] at, int[] size) {
        
    }

    private static ConfigurationSchema loadJson(String fileName) throws IOException {
        File jsonFile = new File(fileName);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        ConfigurationSchema configuration = mapper.readValue(jsonFile, ConfigurationSchema.class);
        return configuration;
    }

}
