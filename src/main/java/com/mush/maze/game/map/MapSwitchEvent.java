/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze.game.map;

/**
 *
 * @author mush
 */
public enum MapSwitchEvent {
    PLAYER_DISAPPEAR,
    MAP_FADE_OUT,
    MAP_FADE_IN,
    PLAYER_REAPPEAR;

    public MapSwitchEvent nextEvent() {
        MapSwitchEvent next = null;
        switch (this) {
            case PLAYER_DISAPPEAR:
                next = MAP_FADE_OUT;
                break;
            case MAP_FADE_OUT:
                next = MAP_FADE_IN;
                break;
            case MAP_FADE_IN:
                next = PLAYER_REAPPEAR;
                break;
            case PLAYER_REAPPEAR:
                next = null;
                break;
        }
        return next;
    }

    public double getDelay() {
        double delay = 0;
        switch (this) {
            case PLAYER_DISAPPEAR:
                delay = 0.1;
                break;
            case MAP_FADE_OUT:
                delay = 0.5;
                break;
            case MAP_FADE_IN:
                delay = 0.1;
                break;
            case PLAYER_REAPPEAR:
                delay = 0.7;
                break;
        }
        return delay;
    }
}
