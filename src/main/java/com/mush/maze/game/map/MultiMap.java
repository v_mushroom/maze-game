/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze.game.map;

import com.mush.game.utils.map.FixedSizeMapContent;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import com.mush.game.utils.map.material.MapMaterialsLoader;
import com.mush.game.utils.map.material.loader.ImageMapLoader;
import com.mush.game.utils.map.move.MapContentCollision;
import com.mush.game.utils.map.move.MapContentItemDimension;
import com.mush.game.utils.tiles.MapContentSurfaceDataSource;
import com.mush.game.utils.tiles.MaterialDataSourceLoader;
import com.mush.game.utils.tiles.TiledSurface;
import com.mush.maze.Config;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class MultiMap {

    public enum UnitDirection {
        U,
        V
    }

    public MapMaterials materials;
    public MapContentCollision<MapMaterial> contentCollision;

    public TiledSurface surface;
    public MapContentSurfaceDataSource<MapMaterial> dataSource;
    private NCubeMap<MapMaterial> nCubeMap;
    private NCubeMap<MapMaterial>.Slice<MapMaterial> mapSlice;

    private int mapIndex;

    public MultiMap(Config config) throws IOException {
        materials = MapMaterialsLoader.loadFromFile("res/materials.json");
        Logger.getGlobal().log(Level.INFO, "materials:{0}", materials.getMaterialNames());

        dataSource = MaterialDataSourceLoader.loadFromFile("res/materials.json", materials);

        loadNCubeMap();

        contentCollision = new MapContentCollision<>(
                new MapContentItemDimension(dataSource.tileWidth, dataSource.tileHeight),
                new ObstacleEvaluator(materials));

        surface = new TiledSurface(
                config.getUnscaledScreenWidth(),
                config.getUnscaledScreenHeight(),
                dataSource.tileWidth,
                dataSource.tileHeight);

        mapIndex = 0;
        applyMap();
    }

    private void loadNCubeMap() throws IOException {
        nCubeMap = NCubeMapActionLoader.loadFromFile("res/ncube-map.json", materials);
        
        /*
        List<FixedSizeMapContent<MapMaterial>> mapContentList = ImageMapLoader.loadListFromFile("res/material-map.json", materials);

        int width = mapContentList.get(0).getWidth();
        int height = mapContentList.get(0).getHeight();
        int depth = mapContentList.size();

        nCubeMap = new NCubeMap<>(width, height, depth);

        for (int w = 0; w < depth; w++) {
            for (int u = 0; u < width; u++) {
                for (int v = 0; v < height; v++) {
                    MapMaterial item = mapContentList.get(w).getItemAt(u, v);
                    nCubeMap.setItemAt(item, u, v, w);
                }
            }
        }
        */

        mapSlice = nCubeMap.getSlice(
                new int[]{0, 0, 0},
                new int[]{1, 0, 0},
                new int[]{0, 1, 0});
    }

    public void nextMap() {
        mapIndex++;
        if (mapIndex >= nCubeMap.getSize(nCubeMap.getN() - 1)) {
            mapIndex = 0;
        }

        mapSlice.getZero()[2] = mapIndex;
        applyMap();
    }

    private void flipToFirstZeroPairAfterNonZero(int[] a, int[] b, int direction) {
        int first = -1;
        int firstAfter = -1;
        int nonZero = -1;
        boolean after = false;

        int start = direction > 0
                ? 0
                : a.length - 1;

        for (int i = 0; i < a.length; i++) {
            int index = start + i * direction;

            if (a[index] == 0 && b[index] == 0) {
                if (first < 0) {
                    first = index;
                }
                if (after) {
                    firstAfter = index;
                    break;
                }
            } else if (a[index] != 0) {
                after = true;
                nonZero = index;
            }
        }

        if (nonZero < 0 || first < 0) {
            return;
        }

        int value = a[nonZero];
        a[nonZero] = 0;
        if (firstAfter < 0) {
            a[first] = -value;
        } else {
            a[firstAfter] = value;
        }
    }

    public void flipMap(Point2D.Double position, UnitDirection unitDirection, int flipDirection) {
        int u = (int) Math.round((position.x - 8) / 16);
        int v = (int) Math.round((position.y - 8) / 16);

        int[] flipPoint = mapSlice.uvToCoordinate(u, v);
        mapSlice.setZero(flipPoint);

        if (unitDirection == UnitDirection.U) {
            flipToFirstZeroPairAfterNonZero(mapSlice.getUVector(), mapSlice.getVVector(), flipDirection);
        } else {
            flipToFirstZeroPairAfterNonZero(mapSlice.getVVector(), mapSlice.getUVector(), flipDirection);
        }

        position.x = 0 + 8;
        position.y = 0 + 8;

        /*
        if (mapSlice.getUVector()[0] == 1) {
            position.x = 16 * mapSlice.getZero()[2] + 8;

            mapSlice.getUVector()[0] = 0;
            mapSlice.getUVector()[2] = 1;

            mapSlice.getZero()[0] = u;
            mapSlice.getZero()[2] = 0;
        } else {
            position.x = 16 * mapSlice.getZero()[0] + 8;
            
            mapSlice.getUVector()[0] = 1;
            mapSlice.getUVector()[2] = 0;

            mapSlice.getZero()[0] = 0;
            mapSlice.getZero()[2] = Math.max(0, Math.min(nCubeMap.getSize(2) - 1, u));
        }
         */
        applyMap();
    }

    private void applyMap() {
        contentCollision.setMapContent(mapSlice);
        dataSource.setMapContent(mapSlice);
        surface.setDataSource(dataSource);
    }

}
