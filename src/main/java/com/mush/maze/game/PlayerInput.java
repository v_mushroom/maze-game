/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.maze.game;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.GameKeyboard;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.TwoInputValue;
import com.mush.game.utils.swing.GameKeyListener;
import com.mush.maze.game.map.MultiMap;
import java.awt.event.KeyEvent;

/**
 *
 * @author mush
 */
public class PlayerInput {

    public enum Event {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        SWITCH
    }

    public class FlipEvent {

        public final MultiMap.UnitDirection unit;
        public final int direction;

        public FlipEvent(MultiMap.UnitDirection unit, int direction) {
            this.unit = unit;
            this.direction = direction;
        }
    }

    private GameKeyboard keyboard;
    private GameKeyListener keyListener;

    public final TwoInputValue frontBack = new TwoInputValue();
    public final TwoInputValue leftRight = new TwoInputValue();

    private MultiMap.UnitDirection flipUnit = MultiMap.UnitDirection.U;
    private int flipDirection = 1;

    public PlayerInput(GameKeyListener keyListener) {
        this.keyListener = keyListener;
        keyboard = new GameKeyboard(GameEventQueue.Categories.INPUT);
        setupKeyboard();
        keyListener.addKeyboard(keyboard);
        GameEventQueue.category(GameEventQueue.Categories.INPUT).addEventListener(this);
    }

    public void clear() {
        keyListener.removeKeyboard(keyboard);
        GameEventQueue.category(GameEventQueue.Categories.INPUT).removeEventListener(this);
    }

    private void setupKeyboard() {
        keyboard.bindStateKey(KeyEvent.VK_A, Event.LEFT);
        keyboard.bindStateKey(KeyEvent.VK_D, Event.RIGHT);
        keyboard.bindStateKey(KeyEvent.VK_W, Event.UP);
        keyboard.bindStateKey(KeyEvent.VK_S, Event.DOWN);

        keyboard.bindStateKey(KeyEvent.VK_LEFT, Event.LEFT);
        keyboard.bindStateKey(KeyEvent.VK_RIGHT, Event.RIGHT);
        keyboard.bindStateKey(KeyEvent.VK_UP, Event.UP);
        keyboard.bindStateKey(KeyEvent.VK_DOWN, Event.DOWN);

        keyboard.bindActionKey(KeyEvent.VK_SPACE, Event.SWITCH);
    }

    @OnGameEvent
    public void onAction(GameInputEvent.State state) {
        if (state.message instanceof Event) {
            switch ((Event) state.message) {
                case UP:
                    frontBack.setInputA(state.active ? 1 : 0);
                    if (state.active) {
                        flipUnit = MultiMap.UnitDirection.V;
                        flipDirection = -1;
                    }
                    break;
                case DOWN:
                    frontBack.setInputB(state.active ? 1 : 0);
                    if (state.active) {
                        flipUnit = MultiMap.UnitDirection.V;
                        flipDirection = 1;
                    }
                    break;
                case LEFT:
                    leftRight.setInputA(state.active ? 1 : 0);
                    if (state.active) {
                        flipUnit = MultiMap.UnitDirection.U;
                        flipDirection = -1;
                    }
                    break;
                case RIGHT:
                    leftRight.setInputB(state.active ? 1 : 0);
                    if (state.active) {
                        flipUnit = MultiMap.UnitDirection.U;
                        flipDirection = 1;
                    }
                    break;
            }
        }
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof Event) {
            switch ((PlayerInput.Event) action.message) {
                case SWITCH:
                    GameEventQueue.category(GameEventQueue.Categories.INPUT).sendEvent(new FlipEvent(flipUnit, flipDirection));
                    break;
            }
        }
    }

}
